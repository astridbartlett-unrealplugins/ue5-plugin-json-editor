using UnrealBuildTool;
using System.IO;

public class BlueprintJson : ModuleRules
{
	public BlueprintJson(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = ModuleRules.PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicIncludePaths.AddRange(new[] { Path.Combine(ModuleDirectory, "./Public") });

		PrivateIncludePaths.AddRange(new[] { Path.Combine(ModuleDirectory, "./Private") });


		PublicDependencyModuleNames.AddRange(
			new string[]
			{
				"Core",
				"InputCore",
				"Json", "JsonUtilities"
			}
			);
			
		
		PrivateDependencyModuleNames.AddRange(
			new string[]
			{
				"CoreUObject",
				"Engine",
				"Slate",
				"SlateCore",
			}
			);
		
		
		DynamicallyLoadedModuleNames.AddRange(
			new string[]
			{
			}
			);
	}
}

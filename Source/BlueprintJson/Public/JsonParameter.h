#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"

#include "JsonParameter.generated.h"

class UJsonParameter;

UENUM(BlueprintType)
enum class EJsonType : uint8
{
	None		UMETA(DisplayName = "None"),
	Null		UMETA(DisplayName = "Null"),
	String		UMETA(DisplayName = "String"),
	Number		UMETA(DisplayName = "Number"),
	Boolean		UMETA(DisplayName = "Boolean"),
	Array		UMETA(DisplayName = "Array"),
	Object		UMETA(DisplayName = "Object")
};

UCLASS(BlueprintType)
class BLUEPRINTJSON_API UJsonParamValue : public UObject
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable, Category="Json|Data")
	FORCEINLINE FString AsString() { return _jsonValue->AsString(); }
	
	UFUNCTION(BlueprintCallable, Category="Json|Data|Check")
	FORCEINLINE bool IsString() { return _jsonValue->Type == EJson::String; }
	
	UFUNCTION(BlueprintCallable, Category="Json|Data")
	FORCEINLINE int AsInt() { return (int) _jsonValue->AsNumber(); }
	
	UFUNCTION(BlueprintCallable, Category="Json|Data|Check")
	FORCEINLINE bool IsInt() { return _jsonValue->Type == EJson::Number; }
	
	UFUNCTION(BlueprintCallable, Category="Json|Data")
	FORCEINLINE float AsFloat() { return (float)_jsonValue->AsNumber(); }
	
	UFUNCTION(BlueprintCallable, Category="Json|Data|Check")
	FORCEINLINE bool IsFloat() { return _jsonValue->Type == EJson::Number; }
	
	UFUNCTION(BlueprintCallable, Category="Json|Data")
	FORCEINLINE float AsBool() { return _jsonValue->AsBool(); }
	
	UFUNCTION(BlueprintCallable, Category="Json|Data|Check")
	FORCEINLINE bool IsBool() { return _jsonValue->Type == EJson::String; }
	
	UFUNCTION(BlueprintCallable, Category="Json|Data")
	UJsonParameter* AsObject();
	
	UFUNCTION(BlueprintCallable, Category="Json|Data|Check")
	FORCEINLINE bool IsObject() { return _jsonValue->Type == EJson::String; }

protected:
	friend class UJsonParameter;

	TSharedPtr<FJsonValue> _jsonValue;
};

UCLASS(BlueprintType)
class BLUEPRINTJSON_API UJsonParameter : public UObject
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable, Category="Json")
	static UJsonParameter* ParseJsonString(FString str);

	UFUNCTION(BlueprintCallable, Category="Json|Data")
	FString GetString(FString key);
	
	UFUNCTION(BlueprintCallable, Category="Json|Data")
	int GetInt(FString key);
	
	UFUNCTION(BlueprintCallable, Category="Json|Data")
	float GetFloat(FString key);
	
	UFUNCTION(BlueprintCallable, Category="Json|Data")
	float GetBool(FString key);
	
	UFUNCTION(BlueprintCallable, Category="Json|Data")
	UJsonParameter* GetObject(FString key);
	
	UFUNCTION(BlueprintCallable, Category="Json|Data")
	TArray<FString> GetStringArray(FString key);
	
	UFUNCTION(BlueprintCallable, Category="Json|Data")
	TArray<int> GetIntArray(FString key);
	
	UFUNCTION(BlueprintCallable, Category="Json|Data")
	TArray<float> GetFloatArray(FString key);
	
	UFUNCTION(BlueprintCallable, Category="Json|Data")
	TArray<bool> GetBoolArray(FString key);
	
	UFUNCTION(BlueprintCallable, Category="Json|Data")
	TArray<UJsonParameter*> GetObjectArray(FString key);
	
	UFUNCTION(BlueprintCallable, Category="Json|Data")
	UJsonParamValue* GetMixed(FString key);
	
	UFUNCTION(BlueprintCallable, Category="Json|Data")
	TArray<UJsonParamValue*> GetMixedArray(FString key);
	
	UFUNCTION(BlueprintCallable, Category="Json|Data|Check")
	bool HasValue(FString key);
	
	UFUNCTION(BlueprintCallable, Category="Json|Data|Check")
	bool HasTypedValue(FString key, EJsonType type);
	
	UFUNCTION(BlueprintCallable, Category="Json|Data|Check")
	bool HasTypedArray(FString key, EJsonType type);
	
protected:
	friend class UJsonParamValue;
	
	TSharedPtr<FJsonObject> GetLowestObject(FString& key);

	TSharedPtr<FJsonObject> _jsonObject;

	FString _rawJsonText;
	FString _fileLocation;
};
#include "JsonParameter.h"

UJsonParameter* UJsonParamValue::AsObject()
{
	auto result = NewObject<UJsonParameter>(this);
	result->_jsonObject = _jsonValue->AsObject();
	return result;
}

UJsonParameter* UJsonParameter::ParseJsonString(FString str)
{
	TSharedPtr<FJsonObject> parsed;
	auto reader = TJsonReaderFactory<TCHAR>::Create(str);

	if (FJsonSerializer::Deserialize(reader, parsed))
	{
		auto result = NewObject<UJsonParameter>();
		result->_jsonObject = parsed;
		return result;
	}

	return nullptr;
}

FString UJsonParameter::GetString(FString key)
{
	return GetLowestObject(key)->GetStringField(key);
}

int UJsonParameter::GetInt(FString key)
{
	return GetLowestObject(key)->GetIntegerField(key);
}

float UJsonParameter::GetFloat(FString key)
{
	return (float)GetLowestObject(key)->GetNumberField(key);
}

float UJsonParameter::GetBool(FString key)
{
	return GetLowestObject(key)->GetBoolField(key);
}

UJsonParameter* UJsonParameter::GetObject(FString key)
{
	auto obj = GetLowestObject(key)->GetObjectField(key);
	auto result = NewObject<UJsonParameter>(this);
	result->_jsonObject = obj;

	return result;
}

TArray<FString> UJsonParameter::GetStringArray(FString key)
{
	TArray<FString> result;
	
	auto raw = GetLowestObject(key)->GetArrayField(key);
	for(auto x : raw)
	{
		result.Add(x->AsString());
	}

	return result;
}

TArray<int> UJsonParameter::GetIntArray(FString key)
{
	TArray<int> result;

	auto raw = GetLowestObject(key)->GetArrayField(key);
	for (auto x : raw)
	{
		result.Add((int)x->AsNumber());
	}

	return result;
}

TArray<float> UJsonParameter::GetFloatArray(FString key)
{
	TArray<float> result;

	auto raw = GetLowestObject(key)->GetArrayField(key);
	for (auto x : raw)
	{
		result.Add((float)x->AsNumber());
	}

	return result;
}

TArray<bool> UJsonParameter::GetBoolArray(FString key)
{
	TArray<bool> result;

	auto raw = GetLowestObject(key)->GetArrayField(key);
	for (auto x : raw)
	{
		result.Add(x->AsBool());
	}

	return result;
}

TArray<UJsonParameter*> UJsonParameter::GetObjectArray(FString key)
{
	TArray<UJsonParameter*> result;

	auto raw = GetLowestObject(key)->GetArrayField(key);
	for (auto x : raw)
	{
		auto param = NewObject<UJsonParameter>(this);
		param->_jsonObject = x->AsObject();
		result.Add(param);
	}

	return result;
}

UJsonParamValue* UJsonParameter::GetMixed(FString key)
{
	auto val = GetLowestObject(key)->GetField<EJson::None>(key);
	auto result = NewObject<UJsonParamValue>(this);
	result->_jsonValue = val;

	return result;
}

TArray<UJsonParamValue*> UJsonParameter::GetMixedArray(FString key)
{
	TArray<UJsonParamValue*> result;

	auto raw = GetLowestObject(key)->GetArrayField(key);
	for (auto x : raw)
	{
		auto param = NewObject<UJsonParamValue>(this);
		param->_jsonValue = x;
		result.Add(param);
	}

	return result;
}

bool UJsonParameter::HasValue(FString key)
{
	return GetLowestObject(key)->HasField(key);
}

bool UJsonParameter::HasTypedValue(FString key, EJsonType type)
{
	auto rawType = (EJson) ((uint8) type);
	auto ptr = GetLowestObject(key)->Values.Find(key);
	if(ptr != nullptr && ptr->IsValid() && (*ptr)->Type == rawType)
	{
		return true;
	}
	
	return false;
}

bool UJsonParameter::HasTypedArray(FString key, EJsonType type)
{
	if(HasTypedValue(key, EJsonType::Array))
	{
		auto rawType = (EJson)((uint8)type);
		
		auto arr = GetLowestObject(key)->GetArrayField(key);
		for(auto x : arr)
		{
			if (x->Type != rawType) return false;
		}
		
		return true;
	}

	return false;
}

TSharedPtr<FJsonObject> UJsonParameter::GetLowestObject(FString& key)
{
	TArray<FString> parts;
	key.ParseIntoArray(parts, _T("."));

	auto currentObj = _jsonObject;

	for (auto i = 0; i < parts.Num() - 1; i++)
	{
		currentObj = _jsonObject->GetObjectField(parts[i]);
	}

	key = parts[parts.Num() - 1];
	return currentObj;
}

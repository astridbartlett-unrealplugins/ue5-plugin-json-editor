#pragma once

#include "CoreMinimal.h"
#include "JsonTextBox.h"

class JSONEDITOR_API FJsonTextCustomization : public IPropertyTypeCustomization
{
public:
	static TSharedRef<IPropertyTypeCustomization> MakeInstance();

private:
	friend class SMultiLineEditableTextBox;
	
public:
	FJsonTextCustomization();
	~FJsonTextCustomization();

public:
	void CustomizeHeader(TSharedRef<IPropertyHandle> PropertyHandle, FDetailWidgetRow& HeaderRow, IPropertyTypeCustomizationUtils& CustomizationUtils) override;
	void CustomizeChildren(TSharedRef<IPropertyHandle> PropertyHandle, IDetailChildrenBuilder& ChildBuilder, IPropertyTypeCustomizationUtils& CustomizationUtils) override;
	
protected:
	TSharedPtr<SJsonTextBox> _textbox;
	TSharedPtr<SHorizontalBox> _buttons;
	TSharedPtr<FButtonStyle> _buttonStyle;
};

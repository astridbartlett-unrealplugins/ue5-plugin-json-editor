#pragma once

#include "CoreMinimal.h"
#include "Widgets/Input/SMultiLineEditableTextBox.h"

class JSONEDITOR_API SJsonTextBox : public SMultiLineEditableTextBox
{
public:
	typedef SMultiLineEditableTextBox Super;
	
public:
	
	FReply OnKeyDown(const FGeometry& geometry, const FKeyEvent& keyEvent) override;
	FReply OnPreviewKeyDown(const FGeometry& geometry, const FKeyEvent& keyEvent) override;

	void Validate();
	void About();
};

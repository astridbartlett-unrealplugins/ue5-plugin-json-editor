﻿#pragma once
#include "Engine/Font.h"

class JSONEDITOR_API Fa
{
public:
	static void InitFont()
	{
		auto fontFile = LoadObject<UFont>(NULL, TEXT("/JsonEditor/FontAwesome_Slim_Font.FontAwesome_Slim_Font"), NULL, LOAD_None, NULL);
		FontAwesomeSlateFont.FontObject = fontFile;
		FontAwesomeSlateFont.Size = 12;
	}

	static FSlateFontInfo GetFont()
	{
		return FontAwesomeSlateFont;
	}

	static FSlateFontInfo GetFont(int size)
	{
		auto font = FontAwesomeSlateFont;
		font.Size = size;
		return font;
	}
	
private:
	inline static FSlateFontInfo FontAwesomeSlateFont{};
	
public:
	inline static FString music {"\x24"};
	inline static FString search {"\x25"};
	inline static FString heart {"\x26"};
	inline static FString star {"\x27"};
	inline static FString star_o {"\x28"};
	inline static FString user {"\x29"};
	inline static FString th {"\x2a"};
	inline static FString check {"\x2b"};
	inline static FString power_off {"\x2c"};
	inline static FString cog {"\x2d"};
	inline static FString trash_o {"\x30"};
	inline static FString home {"\x31"};
	inline static FString road {"\x32"};
	inline static FString refresh {"\x33"};
	inline static FString lock {"\x34"};
	inline static FString volume_off {"\x35"};
	inline static FString volume_down {"\x36"};
	inline static FString volume_up {"\x37"};
	inline static FString tag {"\x38"};
	inline static FString book {"\x39"};
	inline static FString bookmark {"\x3a"};
	inline static FString pencil {"\x3b"};
	inline static FString map_marker {"\x3c"};
	inline static FString ban {"\x3d"};
	inline static FString gift {"\x3e"};
	inline static FString eye {"\x3f"};
	inline static FString eye_slash {"\x40"};
	inline static FString exclamation_triangle {"\x41"};
	inline static FString calendar {"\x42"};
	inline static FString random {"\x43"};
	inline static FString shopping_cart {"\x44"};
	inline static FString key {"\x45"};
	inline static FString heart_o {"\x46"};
	inline static FString thumb_tack {"\x47"};
	inline static FString external_link {"\x48"};
	inline static FString trophy {"\x49"};
	inline static FString bookmark_o {"\x4a"};
	inline static FString certificate {"\x4b"};
	inline static FString globe {"\x4c"};
	inline static FString wrench {"\x4d"};
	inline static FString tasks {"\x4e"};
	inline static FString filter {"\x4f"};
	inline static FString group {"\x50"};
	inline static FString chain {"\x51"};
	inline static FString floppy_o {"\x52"};
	inline static FString bars {"\x53"};
	inline static FString magic {"\x54"};
	inline static FString truck {"\x55"};
	inline static FString rotate_left {"\x56"};
	inline static FString dashboard {"\x57"};
	inline static FString comment_o {"\x58"};
	inline static FString clipboard {"\x59"};
	inline static FString lightbulb_o {"\x5a"};
	inline static FString code {"\x5b"};
	inline static FString star_half_empty {"\x5c"};
	inline static FString info {"\x5d"};
	inline static FString unlock_alt {"\x5e"};
	inline static FString bullseye {"\x5f"};
	inline static FString compass {"\x60"};
	inline static FString thumbs_up {"\x61"};
	inline static FString sun_o {"\x62"};
	inline static FString moon_o {"\x63"};
	inline static FString bug {"\x64"};
	inline static FString wheelchair {"\x65"};
	inline static FString space_shuttle {"\x66"};
	inline static FString cubes {"\x67"};
	inline static FString sliders {"\x68"};
	inline static FString share_alt {"\x69"};
	inline static FString bomb {"\x6a"};
	inline static FString plug {"\x6b"};
	inline static FString line_chart {"\x6c"};
	inline static FString toggle_off {"\x6d"};
	inline static FString toggle_on {"\x6e"};
	inline static FString cc {"\x6f"};
	inline static FString user_secret {"\x70"};
	inline static FString venus {"\x71"};
	inline static FString mars {"\x72"};
	inline static FString transgender_alt {"\x73"};
	inline static FString map_signs {"\x74"};
	inline static FString map_o {"\x75"};
	inline static FString question_circle_o {"\x76"};
	inline static FString blind {"\x77"};
	inline static FString braille {"\x78"};
	inline static FString assistive_listening_systems {"\x79"};
	inline static FString american_sign_language_interpreting {"\x7a"};
	inline static FString deaf {"\x7b"};
	inline static FString user_circle {"\x7c"};
		
	inline static TArray<FString> GetAll()
	{
		return {
			music, search, heart, star, star_o, user, th, check, power_off, cog, trash_o, home, road, refresh, lock,
			volume_off, volume_down, volume_up, tag, book, bookmark, pencil, map_marker, ban, gift, eye, eye_slash,
			exclamation_triangle, calendar, random, shopping_cart, key, heart_o, thumb_tack, external_link, trophy,
			bookmark_o, certificate, globe, wrench, tasks, filter, group, chain, floppy_o, bars, magic, truck, rotate_left,
			dashboard, comment_o, clipboard, lightbulb_o, code, star_half_empty, info, unlock_alt, bullseye, compass,
			thumbs_up, sun_o, moon_o, bug, wheelchair, space_shuttle, cubes, sliders, share_alt, bomb, plug, line_chart,
			toggle_off, toggle_on, cc, user_secret, venus, mars, transgender_alt, map_signs, map_o, question_circle_o,
			blind, braille, assistive_listening_systems, american_sign_language_interpreting, deaf, user_circle,
		};
	}
};
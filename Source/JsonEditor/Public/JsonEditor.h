#pragma once

#include "CoreMinimal.h"
#include "Modules/ModuleManager.h"

class FJsonEditorModule : public IModuleInterface
{
public:
	FJsonEditorModule();
	
public:
	virtual void StartupModule() override;
	virtual void ShutdownModule() override;
};

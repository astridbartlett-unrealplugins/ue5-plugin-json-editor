using UnrealBuildTool;
using System.IO;

public class JsonEditor : ModuleRules
{
	public JsonEditor(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = ModuleRules.PCHUsageMode.UseExplicitOrSharedPCHs;

		CppStandard = CppStandardVersion.Cpp17;

		PublicIncludePaths.AddRange(new[] { Path.Combine(ModuleDirectory, "./Public") });

		PrivateIncludePaths.AddRange(new[] { Path.Combine(ModuleDirectory, "./Private") });


		PublicDependencyModuleNames.AddRange(
			new string[]
			{
				"Core",
				"InputCore",
				"Json", "JsonUtilities"
			}
			);
			
		
		PrivateDependencyModuleNames.AddRange(
			new string[]
			{
				"CoreUObject",
				"Engine",
				"Slate",
				"SlateCore",
				"EditorStyle"
			}
			);
		
		
		DynamicallyLoadedModuleNames.AddRange(
			new string[]
			{
			}
			);
	}
}

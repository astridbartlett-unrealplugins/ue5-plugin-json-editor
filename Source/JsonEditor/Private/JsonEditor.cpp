#include "JsonEditor.h"


#include "Fa.h"
#include "JsonTextCustomization.h"
#include "Engine/Font.h"

#define LOCTEXT_NAMESPACE "FJsonEditorModule"

FJsonEditorModule::FJsonEditorModule()
{
}

void FJsonEditorModule::StartupModule()
{
	Fa::InitFont();
	
	static FName PropertyEditor("PropertyEditor");
	FPropertyEditorModule& PropertyModule = FModuleManager::GetModuleChecked<FPropertyEditorModule>(PropertyEditor);

	PropertyModule.UnregisterCustomPropertyTypeLayout("StrProperty");
	PropertyModule.RegisterCustomPropertyTypeLayout("StrProperty", FOnGetPropertyTypeCustomizationInstance::CreateStatic(&FJsonTextCustomization::MakeInstance));
}

void FJsonEditorModule::ShutdownModule()
{
}

#undef LOCTEXT_NAMESPACE
	
IMPLEMENT_MODULE(FJsonEditorModule, JsonEditor)
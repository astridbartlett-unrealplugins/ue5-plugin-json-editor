#include "JsonTextBox.h"

#include "Misc/MessageDialog.h"

FReply SJsonTextBox::OnKeyDown(const FGeometry& geometry, const FKeyEvent& keyEvent)
{
	if (keyEvent.GetKey() == EKeys::Tab)
	{
		InsertTextAtCursor("\t");
	}
	else
	{
		return Super::OnKeyDown(geometry, keyEvent);
	}
	return FReply::Handled();
}

// Hitting enter doesn't get sent to the OnKeyDown() function for some reason
FReply SJsonTextBox::OnPreviewKeyDown(const FGeometry& geometry, const FKeyEvent& keyEvent)
{
	if (keyEvent.GetKey() == EKeys::Enter)
	{
		FString line;
		GetCurrentTextLine(line);
		FString addlText = "\r\n";
		for (auto x : line)
		{
			if (x == '\t')
			{
				addlText += "\t";
			}
			else
			{
				break;
			}
		}
		InsertTextAtCursor(addlText);
		return FReply::Handled();
	}

	return Super::OnPreviewKeyDown(geometry, keyEvent);
}

void SJsonTextBox::Validate()
{
	auto title = new FText(FText::FromString("JSON Validation"));
	auto text = GetText().ToString();

	if(text.IsEmpty())
	{
		FMessageDialog::Open(EAppMsgType::Type::Ok, FText::FromString("No Content"), title);
		return;
	}
	
	TSharedPtr<FJsonObject> parsed;
	auto reader = TJsonReaderFactory<TCHAR>::Create(text);

	if (FJsonSerializer::Deserialize(reader, parsed))
	{
		FMessageDialog::Open(EAppMsgType::Type::Ok, FText::FromString("All Good!"), title);
	}
	else
	{
		FMessageDialog::Open(EAppMsgType::Type::Ok, FText::FromString("JSON Format Error:\r\n\r\n" + reader->GetErrorMessage()), title);
	}
}

void SJsonTextBox::About()
{
	FString msg = "{\r\n\t\"Author\": {\r\n\t\t\"FirstName\": \"Astrid\",\r\n\t\t\"LastName\": \"Silvers\",\r\n\t\t\"Gender\": \"F\",\r\n\t\t\"Company\": \"ADX Technologies\"\r\n\t},\r\n\t\"Plugin\": {\r\n\t\t\"Name\": \"Unreal JSON Text Editor\",\r\n\t\t\"ModuleName\": \"JsonEditor\",\r\n\t\t\"CreatedYear\": 2022,\r\n\t\t\"UnrealEngineVersion\": \"5.0.0+\",\r\n\t\t\"Description\": \"Adds a JSON text editor inside the Unreal Engine Editor\",\r\n\t\t\"Usage\": {\r\n\t\t\t\"Description\": \"Add 'Json' to FString UPROPERTY meta\",\r\n\t\t\t\"Example\": \"UPROPERTY(EditAnywhere, meta = (Json))\\r\\nFString JsonProperties;\"\r\n\t\t}\r\n\t},\r\n\t\"AdditionalIncludes\": [\r\n\t\t{\r\n\t\t\t\"Type\": \"Font\",\r\n\t\t\t\"Name\": \"FontAwesome_Slim\",\r\n\t\t\t\"Description\": \"A Font that is made up of icons. Due to UE limitations, this has been drastically slimmed down from the full font\",\r\n\t\t\t\"DerivedFrom\": \"Font Awesome (https://fontawesome.com/)\"\r\n\t\t},\r\n\t\t{\r\n\t\t\t\"Type\": \"BlueprintType\",\r\n\t\t\t\"Name\": \"JsonParameters\",\r\n\t\t\t\"Description\": \"A Blueprint Type for the purpose of handling JSON objects\"\r\n\t\t}\r\n\t]\r\n}";
	FMessageDialog::Open(EAppMsgType::Type::Ok, FText::FromString(msg), new FText(FText::FromString("About JsonEditor")));
}

﻿#include "JsonTextCustomization.h"

#include "DetailWidgetRow.h"
#include "Fa.h"
#include "JsonTextBox.h"
#include "Engine/Font.h"
#include "Misc/MessageDialog.h"
#include "Widgets/Input/SEditableTextBox.h"
#include "Widgets/Input/SMultiLineEditableTextBox.h"
#include "Widgets/Text/SInlineEditableTextBlock.h"


TSharedRef<IPropertyTypeCustomization> FJsonTextCustomization::MakeInstance()
{
	return MakeShareable(new FJsonTextCustomization());
}

FJsonTextCustomization::FJsonTextCustomization()
{
}

FJsonTextCustomization::~FJsonTextCustomization()
{
}

void FJsonTextCustomization::CustomizeHeader(TSharedRef<IPropertyHandle> propertyHandle, FDetailWidgetRow& headerRow,
	IPropertyTypeCustomizationUtils& customizationUtils)
{
	FString currVal;
	propertyHandle->GetPerObjectValue(0, currVal);
	
	if(propertyHandle->HasMetaData("Json"))
	{
		FMargin buttonPadding(10, 5);
		
		_buttonStyle = MakeShareable(new FButtonStyle(FButtonStyle::GetDefault()));
		FSlateBrush emptyBrush;
		_buttonStyle->SetNormal(emptyBrush);
		_buttonStyle->SetHovered(emptyBrush);
		_buttonStyle->SetPressed(emptyBrush);
		_buttonStyle->SetDisabled(emptyBrush);

		auto buttonColor = FColor::Purple;
		_buttonStyle->SetHoveredForeground(FSlateColor(buttonColor));
		
		buttonColor.R = FMath::Max(0, buttonColor.R - 35);
		buttonColor.G = FMath::Max(0, buttonColor.G - 35);
		buttonColor.B = FMath::Max(0, buttonColor.B - 35);
		
		_buttonStyle->SetPressedForeground(FSlateColor(buttonColor));

		propertyHandle->SetOnPropertyResetToDefault(FSimpleDelegate::CreateLambda([this, propertyHandle]()
		{
			FString defaultVal;
			propertyHandle->GetPerObjectValue(0, defaultVal);
			_textbox->SetText(FText::FromString(defaultVal));
			_textbox->SetVisibility(EVisibility::Visible);
		}));
		
		headerRow.FilterString(propertyHandle->GetPropertyDisplayName())
			.NameContent()
			[
				propertyHandle->CreatePropertyNameWidget()
			]
		.ValueContent()
			.MinDesiredWidth(125.f)
			.MaxDesiredWidth(600.f)
			[
				SNew(SVerticalBox)
					+SVerticalBox::Slot()
					.HAlign(EHorizontalAlignment::HAlign_Left).AutoHeight()
					[
						SNew(SHorizontalBox)
							// Show/Hide button
							+SHorizontalBox::Slot()
							.VAlign(EVerticalAlignment::VAlign_Center).AutoWidth().Padding(buttonPadding)
							[
								SNew(SButton)
								.ButtonStyle(_buttonStyle.Get())
								.OnPressed_Lambda([this]()
								{
									if(!_textbox.IsValid()) return;
									
									if (_textbox->GetVisibility() == EVisibility::Visible)
									{
										_textbox->SetVisibility(EVisibility::Collapsed);
									}
									else
									{
										_textbox->SetVisibility(EVisibility::Visible);
									}
								})
								.ToolTipText(FText::FromString("Show/Hide"))
								.ContentPadding(FMargin(2))
								.Cursor(EMouseCursor::Type::Hand)
								.ButtonColorAndOpacity(FSlateColor(FColor::Transparent))
								[
									SNew(STextBlock)
										.Font(Fa::GetFont(13))
										.Text_Lambda([this]()
										{
											if(!_textbox.IsValid()) return FText::FromString(Fa::eye);
											return FText::FromString(_textbox->GetVisibility() == EVisibility::Visible ? Fa::eye : Fa::eye_slash);
										})
										.Margin(FMargin(0))
								]
							]
							// Validate Button
							+SHorizontalBox::Slot()
							.VAlign(EVerticalAlignment::VAlign_Center).AutoWidth().Padding(buttonPadding)
							[
								SNew(SButton)
									.ButtonStyle(_buttonStyle.Get())
									.OnPressed_Lambda([this]()
									{
										if(!_textbox.IsValid()) return;
										
										_textbox->Validate();
									})
									.ToolTipText(FText::FromString("Validate"))
									.ContentPadding(FMargin(2))
									.Cursor(EMouseCursor::Type::Hand)
									.ButtonColorAndOpacity(FSlateColor(FColor::Transparent))
									[
										SNew(STextBlock)
											.Font(Fa::GetFont(13))
											.Text(FText::FromString(Fa::check))
											.Margin(FMargin(0))
									]
							]
							// About Button
							+SHorizontalBox::Slot()
							.VAlign(EVerticalAlignment::VAlign_Center).AutoWidth().Padding(buttonPadding)
							[
								SNew(SButton)
									.ButtonStyle(_buttonStyle.Get())
									.OnPressed_Lambda([this]()
									{
										if(!_textbox.IsValid()) return;
										
										_textbox->About();
									})
									.ToolTipText_Lambda([propertyHandle]()
									{
										if(!propertyHandle->IsValidHandle()) return FText::FromString("");
										
										FString val;
										propertyHandle->GetPerObjectValue(0, val);
										return FText::FromString(val);
									})
									.ContentPadding(FMargin(2))
									.ButtonColorAndOpacity(FSlateColor(FColor::Transparent))
									[
										SNew(STextBlock)
											.Font(Fa::GetFont(13))
											.Text(FText::FromString(Fa::info))
											.Margin(FMargin(0))
									]
							]
					]
					+SVerticalBox::Slot()
					.MaxHeight(500)
					[
						SAssignNew(_textbox, SJsonTextBox)
							.Text(FText::FromString(currVal))
							.OnTextChanged_Lambda([propertyHandle](const FText& newText)
							{
								if (!propertyHandle->IsValidHandle()) return;
								
								propertyHandle->SetPerObjectValue(0, newText.ToString());
							})
					]

			];
		
		TArray<FString> lines;
		currVal.ParseIntoArrayLines(lines);
		if(lines.Num() > 4)
		{
			_textbox->SetVisibility(EVisibility::Collapsed);
		}
	}
	else
	{
		headerRow.FilterString(propertyHandle->GetPropertyDisplayName())
			.NameContent()
			[
				propertyHandle->CreatePropertyNameWidget()
			]
			.ValueContent()
			.MinDesiredWidth(125.f)
			.MaxDesiredWidth(600.f)
			[
				SNew(SEditableTextBox)
					.Text(FText::FromString(currVal))
					.Font(FEditorStyle::GetFontStyle("PropertyWindow.NormalFont"))
					.OnTextChanged_Lambda([propertyHandle](const FText& newText)
					{
						propertyHandle->SetPerObjectValue(0, newText.ToString());
					})
			];
	}
}


void FJsonTextCustomization::CustomizeChildren(TSharedRef<IPropertyHandle> propertyHandle,
	IDetailChildrenBuilder& childBuilder, IPropertyTypeCustomizationUtils& customizationUtils)
{
	if (propertyHandle->HasMetaData("Json"))
	{
	}
	else
	{
	}
}
